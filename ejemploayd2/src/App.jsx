import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import ansiblevspuppetLogo from './assets/ansiblevspuppet.jpeg';
import './App.css'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <div>
        <img src={ansiblevspuppetLogo}/>
      </div>
      <h1>Ansible y Puppet como Herramientas de Automatización de Despliegues</h1>
      <p className="read-the-docs">
        Conferencia impartida por Ing. Erick Lemus
      </p>
    </>
  )
}

export default App
