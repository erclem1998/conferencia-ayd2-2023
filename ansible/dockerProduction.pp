node 'produccion' {
  file { '/home/ansible/docker-compose.production.yml':
    source => 'puppet:///modules/module_name/docker-compose.production.yml',
    mode   => '0644',
    owner  => 'ansible',
    group  => 'ansible',
  }

  exec { 'ejecutar despliegue de docker-compose':
    command => 'docker-compose -f /home/ansible/docker-compose.production.yml up --build -d',
    user    => 'ansible',
    group   => 'ansible',
    require => File['/home/ansible/docker-compose.production.yml'],
  }
}
